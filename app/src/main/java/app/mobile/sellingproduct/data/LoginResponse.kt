package app.mobile.sellingproduct.data

data class LoginResponse(
    val code: Int,
    val message: String,
    val data: UserData
)

data class UserData(
    val userId: Int,
    val name: String,
    val phone: String,
    val userPermit: UserPermit
)

data class UserPermit(
    val jabatan: String
)
