package app.mobile.sellingproduct.data

data class LoginRequest(
    val phone: String,
    val password: String
)