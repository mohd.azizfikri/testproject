package app.mobile.sellingproduct.utils

import app.mobile.sellingproduct.data.LoginRequest
import app.mobile.sellingproduct.data.LoginResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {
    @POST("api/mobile/v1/login")
    suspend fun loginUser(@Body request: LoginRequest): Response<LoginResponse>
}