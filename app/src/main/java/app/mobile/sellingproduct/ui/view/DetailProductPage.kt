package app.mobile.sellingproduct.ui.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import app.mobile.sellingproduct.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailProductPage(product: Product) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Detail Product") },
                navigationIcon = {
                    IconButton(onClick = { /* Handle back button action */ }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "Back")
                    }
                }
            )
        }
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp, it.calculateTopPadding())
                .fillMaxSize()
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                Column(
                    modifier = Modifier.weight(1f)
                ) {
                    Text(text = "Nama Product", fontWeight = FontWeight.Bold)
                    Spacer(modifier = Modifier.height(4.dp))
                    Text(text = "Ukuran produk (${product.size} ml)")
                }
                Image(
                    painter = painterResource(id = product.imageRes),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .size(120.dp)
                        .clip(RoundedCornerShape(8.dp))
                )
            }

            Spacer(modifier = Modifier.height(16.dp))

            Text(text = "Harga Barang: ${product.price}")
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = "Jumlah Stok: ${product.stock}")
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = "Deskripsi: ${product.description}")
        }
    }
}

fun getSampleProduct(): Product {
    return Product(
        imageRes = R.drawable.ic_profile,
        title = "Sample Product",
        size = 200,
        price = "$19.99",
        stock = 100,
        description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    )
}