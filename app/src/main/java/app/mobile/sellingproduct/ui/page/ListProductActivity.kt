package app.mobile.sellingproduct.ui.page

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.MaterialTheme
import app.mobile.sellingproduct.R
import app.mobile.sellingproduct.ui.view.ListProductPage
import app.mobile.sellingproduct.ui.view.Product

class ListProductActivity: ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                // Display AddProductPage composable
                val sampleProducts = listOf(
                    Product(R.drawable.ic_profile, "Product 1"),
                    Product(R.drawable.ic_profile, "Product 2"),
                    Product(R.drawable.ic_profile, "Product 3"),
                    Product(R.drawable.ic_profile, "Product 4"),
                    Product(R.drawable.ic_profile, "Product 5"),
                    Product(R.drawable.ic_profile, "Product 6"),
                    Product(R.drawable.ic_profile, "Product 7"),
                    Product(R.drawable.ic_profile, "Product 3"),
                    Product(R.drawable.ic_profile, "Product 4"),
                    Product(R.drawable.ic_profile, "Product 5"),
                    Product(R.drawable.ic_profile, "Product 5"),
                    Product(R.drawable.ic_profile, "Product 6"),
                    Product(R.drawable.ic_profile, "Product 7"),
                    Product(R.drawable.ic_profile, "Product 3"),
                    Product(R.drawable.ic_profile, "Product 4"),
                    Product(R.drawable.ic_profile, "Product 5"),
                    Product(R.drawable.ic_profile, "Product 6"),
                    Product(R.drawable.ic_profile, "Product 7")
                )

                ListProductPage(products = sampleProducts){
                    val intent = Intent(applicationContext, DetailProductActivity::class.java)
                    startActivity(intent)

                }
            }
        }
    }
}