package app.mobile.sellingproduct.ui.page

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.MaterialTheme
import app.mobile.sellingproduct.ui.view.LoginPage

class LoginActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                LoginPage(
                    onRegisterClicked = {
                        startActivity(RegisterActivity.createIntent(this))
                    }
                )
            }
        }
    }
}
