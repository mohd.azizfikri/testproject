package app.mobile.sellingproduct.ui.page

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.*
import app.mobile.sellingproduct.ui.view.VerifyInformationPage

class VerifyInformationActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                // Display VerifyInformationPage composable
                VerifyInformationPage()
            }
        }
    }
}