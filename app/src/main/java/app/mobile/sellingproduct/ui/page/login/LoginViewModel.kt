package app.mobile.sellingproduct.ui.page.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.mobile.sellingproduct.data.LoginRequest
import app.mobile.sellingproduct.data.LoginResponse
import app.mobile.sellingproduct.utils.ApiService
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val apiService: ApiService
): ViewModel()  {

    private val _profileData = MutableLiveData<LoginRequest>()
    val profileData: LiveData<LoginRequest> = _profileData

    private val _loginResult = MutableLiveData<LoginResponse>()
    val loginResult: LiveData<LoginResponse> = _loginResult

    // New state to track loading status
    private val _isLoggingIn = MutableStateFlow(false)
    val isLoggingIn: StateFlow<Boolean> = _isLoggingIn


    fun loginUser(phone: String, password: String) {
        viewModelScope.launch {
            _isLoggingIn.value = true // Set loading status to true

            val request = LoginRequest(phone, password)
            val response = apiService.loginUser(request)

            _loginResult.value = response.body()

            _isLoggingIn.value = false
        }
    }
}