package app.mobile.sellingproduct.ui.view

import android.annotation.SuppressLint
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.mobile.sellingproduct.R

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MenuPage(
   /* onMenu1Clicked: () -> Unit,
    onMenu2Clicked: () -> Unit,
    onMenu3Clicked: () -> Unit*/
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "") },
                actions = {
                    // Profile image in the top right corner
                    Image(
                        painter = painterResource(id = R.drawable.ic_profile),
                        contentDescription = "Profile",
                        modifier = Modifier
                            .padding(8.dp)
                            .size(32.dp)
                            .clip(CircleShape)
                            .background(MaterialTheme.colorScheme.primary)
                    )
                }
            )
        }
    ) {
        // Create a list of menu items
        val menuItems = listOf(
            MenuItemData(R.drawable.menu_verification, "Verifikasi Faktur"),
            MenuItemData(R.drawable.menu_add, "Tambah Produk"),
            MenuItemData(R.drawable.menu_list, "Daftar Produk"),
            // Add more menu items as needed
        )

        Column() {
            Spacer(modifier = Modifier.padding(24.dp))
            LazyVerticalGrid(
                columns = GridCells.Fixed(4),
                modifier = Modifier.fillMaxSize()
            ) {
                items(menuItems) { menuItem ->
                    MenuItem(menuItem)
                }
            }
        }
        // Display menu items in a grid with a maximum of 4 items per row

    }
}

@Composable
fun MenuItem(menuItem: MenuItemData) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .padding(16.dp)
            .width(IntrinsicSize.Max)
    ) {
        Image(
            painter = painterResource(id = menuItem.iconResId),
            contentDescription = menuItem.title,
            modifier = Modifier
                .size(48.dp)
                .aspectRatio(1f) // Ensure the image maintains a square aspect ratio
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(text = menuItem.title, fontSize = 16.sp)
    }
}

data class MenuItemData(
    @DrawableRes val iconResId: Int,
    val title: String
)
