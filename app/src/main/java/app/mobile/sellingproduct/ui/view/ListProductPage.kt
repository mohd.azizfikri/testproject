package app.mobile.sellingproduct.ui.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ListProductPage(products: List<Product>, navigateToDetail: (Product) -> Unit) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "List Product") }
            )
        }
    ) {
        Column {
            // Space
            Spacer(modifier = Modifier.padding(it.calculateBottomPadding(), 28.dp))

            // Content
            LazyVerticalGrid(columns = GridCells.Fixed(3)) {
                items(products) { product ->
                    ProductItem(product, navigateToDetail)
                }
            }
        }
    }
}

@Composable
fun ProductItem(product: Product, navigateToDetail: (Product) -> Unit) {
    Card(
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier
                .clickable { navigateToDetail(product) }
                .padding(8.dp)
        ) {
            Image(
                painter = painterResource(id = product.imageRes),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(120.dp)
                    .clip(RoundedCornerShape(8.dp))
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = product.title, fontWeight = FontWeight.Bold)
        }
    }
}

data class Product(
    val imageRes: Int,
    val title: String,
    val size: Int = 200,
    val price: String = "Rp 9700",
    val stock: Int = 100,
    val description: String = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
)

