package app.mobile.sellingproduct.ui.view

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
/*
fun VerificationPage() {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Verification Page") },
                navigationIcon = {
                    IconButton(onClick = { */
/* Handle back button click *//*
 }) {
                        Icon(Icons.Filled.ArrowBack, contentDescription = "Back Button")
                    }
                }
            )
        }
    ) {
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            contentPadding = PaddingValues(horizontal = 16.dp, vertical = it.calculateTopPadding())
        ) {
            itemsIndexed(listOf("Box 1", "Box 2", "Box 3", "Box 4")) { index, box ->
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                        .background(Color.LightGray, shape = RoundedCornerShape(8.dp))
                ) {
                    Column(modifier = Modifier.padding(8.dp)) {
                        Text(
                            text = box,
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Bold
                        )
                        Text(
                            text = "Text $index",
                            fontSize = 16.sp,
                            fontWeight = FontWeight.Normal
                        )
                        Text(
                            text = "Text $index",
                            fontSize = 16.sp,
                            fontWeight = FontWeight.Normal
                        )
                    }
                }
            }
        }
    }
}*/


fun VerificationPage() {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Verification") },
                navigationIcon = {
                    IconButton(onClick = { /* Handle back button action */ }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "Back")
                    }
                }
            )
        }
    ) {
        val verificationItems = listOf(
            VerificationItem("Code 123", "John Doe", "johndoe@example.com"),
            VerificationItem("Code 456", "Jane Smith", "janesmith@example.com"),
            // Add more verification items as needed
        )

        LazyColumn(contentPadding = PaddingValues(horizontal = 16.dp, vertical = it.calculateTopPadding())) {
            items(verificationItems) { item ->
                VerificationItemBox(item)
                Divider() // Add a divider between items
            }
        }
    }
}

@Composable
fun VerificationItemBox(item: VerificationItem) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        Column {
            Text(text = item.code, fontWeight = FontWeight.Bold)
            Text(text = item.name)
            Text(text = item.email)
        }
    }
}

data class VerificationItem(
    val code: String,
    val name: String,
    val email: String
)