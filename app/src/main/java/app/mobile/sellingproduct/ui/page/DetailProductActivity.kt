package app.mobile.sellingproduct.ui.page

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.MaterialTheme
import app.mobile.sellingproduct.ui.view.DetailProductPage
import app.mobile.sellingproduct.ui.view.getSampleProduct

class DetailProductActivity : ComponentActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val product = getSampleProduct() // Get a sample product
        setContent {
            MaterialTheme {
                DetailProductPage(product = product)
            }
        }
    }
}