package app.mobile.sellingproduct.ui.page

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.MaterialTheme
import app.mobile.sellingproduct.ui.view.MenuPage

class MenuActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                MenuPage()
            }
        }
    }

    companion object {
        fun createIntent(context: Context): Intent {
            return Intent(context, MenuActivity::class.java)
        }
    }
}