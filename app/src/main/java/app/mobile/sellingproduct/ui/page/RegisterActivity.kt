package app.mobile.sellingproduct.ui.page

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.MaterialTheme
import app.mobile.sellingproduct.ui.view.RegisterPage

class RegisterActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                RegisterPage()
            }
        }
    }

    companion object {
        fun createIntent(context: Context): Intent {
            return Intent(context, RegisterActivity::class.java)
        }
    }
}