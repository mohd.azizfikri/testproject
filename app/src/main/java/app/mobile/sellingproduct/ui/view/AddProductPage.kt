package app.mobile.sellingproduct.ui.view

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddProductPage() {

    val namaBarangValue = remember { mutableStateOf("") }
    val ukuranBarangValue = remember { mutableStateOf("") }
    val jumlahBarangValue = remember { mutableStateOf("") }
    val deskripsiValue = remember { mutableStateOf("") }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Tambah Barang") },
                navigationIcon = {
                    IconButton(onClick = { /* Handle back button action */ }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "Back")
                    }
                }
            )
        }
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp, it.calculateTopPadding())
                .fillMaxSize()
        ) {
            val selectedRadioButton = remember { mutableStateOf(0) }
/*
            // Radio Buttons
            RadioGroup(
                options = listOf("Tambah Barang Baru", "Tambah Barang Lama"),
                selectedOption = selectedRadioButton.value,
                onOptionSelected = { selectedRadioButton.value = it }
            )*/

            Column {
                RadioOption(
                    label = "Tambah Barang Baru",
                    selected = selectedRadioButton.value == 0,
                    onSelect = { selectedRadioButton.value = 0 }
                )
                Spacer(modifier = Modifier.height(8.dp))
                RadioOption(
                    label = "Tambah Barang Lama",
                    selected = selectedRadioButton.value == 1,
                    onSelect = { selectedRadioButton.value = 1 }
                )
            }

            Spacer(modifier = Modifier.height(16.dp))

            // Input fields based on selected radio button
            when (selectedRadioButton.value) {
                0 -> {
                    // Tambah Barang Baru
                    TextField(
                        value = namaBarangValue.value,
                        onValueChange = { newName -> namaBarangValue.value = newName },
                        label = { Text("Nama Barang") }
                    )
                }
                1 -> {
                    // Tambah Barang Lama
                    DropdownField(
                        label = "Pilih Barang",
                        items = listOf("Essens Aroma", "Lavender Taste"),
                        selectedItem = "Essens Aroma",
                        onItemSelected = { /*selectedItem.value = it*/ }
                    )
                }
            }

            Spacer(modifier = Modifier.height(16.dp))

            // Input fields
            TextField(
                value = ukuranBarangValue.value,
                onValueChange = { newSize -> ukuranBarangValue.value = newSize },
                label = { Text("Ukuran barang (ml)") }
            )
            Spacer(modifier = Modifier.height(8.dp))
            TextField(
                value = jumlahBarangValue.value,
                onValueChange = { newJumlah -> jumlahBarangValue.value = newJumlah },
                label = { Text("Jumlah barang") }, /* Other TextField parameters */
            )
            Spacer(modifier = Modifier.height(8.dp))
            TextField(
                value = deskripsiValue.value,
                onValueChange = { newDesc -> deskripsiValue.value = newDesc },
                label = { Text("Deskripsi") }, /* Other TextField parameters */
            )

            Spacer(modifier = Modifier.height(16.dp))

            // "Tambah" button
            Button(
                onClick = { /* Handle Tambah button click */ },
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(text = "Tambah")
            }
        }
    }
}

@Composable
fun RadioOption(
    label: String,
    selected: Boolean,
    onSelect: () -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onSelect() }
    ) {
        RadioButton(
            selected = selected,
            onClick = { onSelect() }
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(text = label)
    }
}
