package app.mobile.sellingproduct.ui.view

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.mobile.sellingproduct.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VerifyInformationPage() {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Information") },
                navigationIcon = {
                    IconButton(onClick = { /* Handle back button action */ }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "Back")
                    }
                }
            )
        }
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp, it.calculateTopPadding())
                .fillMaxSize()
        ) {
            // Title-Value Pairs
            TitleValuePair(title = "Nama Sales", value = "John Doe")
            Spacer(modifier = Modifier.height(16.dp))
            TitleValuePair(title = "Jenis Faktur", value = "Retail")
            Spacer(modifier = Modifier.height(32.dp))

            // List of Product Items
            Text(text = "Produk", fontWeight = FontWeight.Bold, fontSize = 18.sp)
            Spacer(modifier = Modifier.height(8.dp))

            // Display the InteractiveItemList
            InteractiveItemList()

            // Konfirmasi Button
            Spacer(modifier = Modifier.height(16.dp))
            Button(
                onClick = { /* Handle Konfirmasi button click */ },
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(text = "Konfirmasi")
            }
        }
    }
}

@Composable
fun TitleValuePair(title: String, value: String) {
    Column {
        Text(text = title, fontWeight = FontWeight.Bold, fontSize = 16.sp)
        Text(text = value)
    }
}

@Composable
fun ListItem(text: String) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(vertical = 4.dp)
    ) {
        Icon(
            imageVector = Icons.Default.AccountCircle,
            contentDescription = null,
            modifier = Modifier
                .size(8.dp)
                .padding(end = 8.dp)
        )
        Text(text = text)
    }
}


@Composable
fun InteractiveItemList() {
    val itemList = listOf(
        InteractiveItem(
            imageResId = R.drawable.ic_profile,
            title = "Item 1",
            subtitle = "Subtitle 1",
            date = "2023-08-30"
        ),
        InteractiveItem(
            imageResId = R.drawable.ic_profile,
            title = "Item 2",
            subtitle = "Subtitle 2",
            date = "2023-08-31"
        ),
        // Add more items as needed
    )

    LazyColumn {
        items(itemList) { item ->
            InteractiveItemCard(item)
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}

@Composable
fun InteractiveItemCard(item: InteractiveItem) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .shadow(4.dp)
    ) {
        Row(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = painterResource(id = item.imageResId),
                contentDescription = "Item Image",
                modifier = Modifier.size(48.dp)
            )
            Spacer(modifier = Modifier.width(16.dp))
            Column {
                Text(text = item.title, fontWeight = FontWeight.Bold)
                Text(text = item.subtitle)
            }
            Spacer(modifier = Modifier.weight(1f))
            Text(text = item.date)
        }
    }
}

data class InteractiveItem(
    @DrawableRes val imageResId: Int,
    val title: String,
    val subtitle: String,
    val date: String
)
