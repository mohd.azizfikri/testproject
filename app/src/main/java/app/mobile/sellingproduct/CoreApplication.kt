package app.mobile.sellingproduct

import android.app.Application
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.ViewModelStoreOwner
import app.mobile.sellingproduct.utils.ApiService
import app.mobile.sellingproduct.utils.RetrofitInstance
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoreApplication : Application(), ViewModelStoreOwner {

    private lateinit var viewModelStore: ViewModelStore

    lateinit var apiService: ApiService

    override fun onCreate() {
        super.onCreate()

        viewModelStore = ViewModelStore()
        apiService = RetrofitInstance.retrofit.create(ApiService::class.java)
    }

    override fun getViewModelStore(): ViewModelStore {
        return viewModelStore
    }
}